import Shop.*;

public class Main {
    public static void main(String[] args) {

        Shop ap = new Apple(15);
        Shop ri = new Rice(4.5);

        Constructor constructor = new Constructor();

//      System.out.println("Apple: " + ap.findCost());
//      System.out.println("Rice: " + ri.findCost());
//      System.out.println("Total price:"+ ap+ri);

        IHandleShop[] shops = {(IHandleShop) ap,
                (IHandleShop) ri,
                (IHandleShop) constructor};
        for (IHandleShop i : shops) { // : - foreach
            System.out.println(i.findCost());
        }

    }
}
