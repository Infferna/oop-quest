package Shop;

public class Apple extends Shop implements IHandleShop{

    private double count;

    public Apple () {}
    public Apple(double c) {
        this.count = c;
    }

    public double getCount() {
        return count;
    }

    public void setCount(double c) {
        this.count = c;
    }

    public double findCost(){
        price = findCostFine();
        return price;
    }

    private double findCostFine(){
        return count*10;
    }

}
