package Shop;

public class Rice extends Shop implements IHandleShop{

    private double kg;

    public Rice(){}
    public Rice(double c) {
        this.kg = c;
    }

    public double getKg(double c) {
        return kg;
    }

    public void setKg(double c) {
        this.kg = c;
    }

    public double findCost(){
        price = kg*15;
        return price;
    }
}
