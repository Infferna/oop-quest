package Shop;

abstract public class Shop {

    protected double price;

    public void printShop () {
        System.out.println("Shop");
    }

    public abstract double findCost();
}
